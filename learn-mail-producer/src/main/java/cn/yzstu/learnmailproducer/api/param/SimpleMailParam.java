package cn.yzstu.learnmailproducer.api.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author baldwin
 */
@Data
@ApiModel(value = "发送简单邮件")
public class SimpleMailParam {

    @ApiModelProperty(value = "发送到",example = "1240594179@qq.com")
    private String mailTo;

    @ApiModelProperty(value = "标题",example = "测试邮件标题")
    private String subject;

    @ApiModelProperty(value = "内容",example = "测试邮件内容")
    private String content;

}
