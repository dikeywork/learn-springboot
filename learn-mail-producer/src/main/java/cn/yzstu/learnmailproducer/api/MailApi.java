package cn.yzstu.learnmailproducer.api;

import cn.yzstu.learnmailproducer.api.param.SimpleMailParam;
import cn.yzstu.learnmailproducer.util.MailUtil;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author baldwin
 */
@RestController
@RequestMapping(value = "/send")
public class MailApi {

    @Autowired
    private MailUtil mailUtil;

    @PostMapping(value = "/simpleMail")
    @ApiOperation(value = "发送简单邮件")
    public String simpleMail(@ApiParam @RequestBody SimpleMailParam simpleMail){
        mailUtil.sendSimpleMail(simpleMail.getMailTo(),simpleMail.getSubject(),simpleMail.getContent());
        return "调用成功";
    }

}
