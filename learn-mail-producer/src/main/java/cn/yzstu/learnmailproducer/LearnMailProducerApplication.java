package cn.yzstu.learnmailproducer;

import cn.yzstu.learnmailproducer.util.MailUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearnMailProducerApplication {
    @Autowired
    private MailUtil mailUtil;

    public static void main(String[] args) {
        SpringApplication.run(LearnMailProducerApplication.class, args);
    }

}
