package cn.yzstu.learnmailproducer;

import cn.yzstu.learnmailproducer.util.MailUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class LearnMailProducerApplicationTests {

    @Autowired
    private MailUtil mailUtil;

    @Test
    void contextLoads() {
        mailUtil.sendSimpleMail("1240594179@qq.com","测试","测试信息");
    }

}
