package cn.yzstu.learnnacos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author baldwin
 */
@SpringBootApplication
public class LearnNacosApplication {
    public static void main(String[] args) {
        SpringApplication.run(LearnNacosApplication.class, args);
    }
}
