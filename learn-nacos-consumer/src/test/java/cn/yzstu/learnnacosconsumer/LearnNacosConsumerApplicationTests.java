package cn.yzstu.learnnacosconsumer;

import cn.yzstu.learnmailproducer.util.MailUtil;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class LearnNacosConsumerApplicationTests {

    @Test
    void contextLoads() {
        MailUtil mailUtil = new MailUtil();
        mailUtil.sendSimpleMail("1240594179@qq.com","测试","测试信息");
    }

}
