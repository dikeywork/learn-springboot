package cn.yzstu.learnnacosconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 项目启动文件
 * @author baldwin
 */
@SpringBootApplication
public class LearnNacosConsumerApplication {
    public static void main(String[] args) {
        SpringApplication.run(LearnNacosConsumerApplication.class, args);
    }

}
