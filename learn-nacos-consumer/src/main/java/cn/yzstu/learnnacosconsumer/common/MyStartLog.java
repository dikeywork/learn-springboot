package cn.yzstu.learnnacosconsumer.common;

import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author baldwin
 */
@Component
@Order(value = 1)
public class MyStartLog implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {
        StringBuilder commandLog = new StringBuilder();
        commandLog.append("+==================================================================================+\n");
        commandLog.append("+                        项目启动成功!!!                                             +\n");
        commandLog.append("+        我的博客地址:https://yzstu.blog.csdn.net                                    +\n");
        commandLog.append("+        当前项目地址:https://gitee.com/dikeywork/learn-springboot/                  +\n");
        commandLog.append("+        系列文章地址:https://blog.csdn.net/shouchenchuan5253/category_10223260.html +\n");
        commandLog.append("+==================================================================================+\n");
        System.out.println(commandLog.toString());
    }
}
