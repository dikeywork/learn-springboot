package cn.yzstu.yzstubase.yzstubase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YzstuBaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(YzstuBaseApplication.class, args);
    }

}
