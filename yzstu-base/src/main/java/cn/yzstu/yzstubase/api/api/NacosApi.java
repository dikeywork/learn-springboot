package cn.yzstu.yzstubase.api.api;

import com.alibaba.nacos.api.annotation.NacosInjected;
import com.alibaba.nacos.api.config.annotation.NacosValue;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.api.naming.pojo.Instance;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author baldwin
 */
@RestController
@RequestMapping(("/api/nacos"))
@Api(value = "Nacos服务相关接口")
@Data
public class NacosApi {

    @NacosInjected
    private NamingService namingService;

    @NacosValue(value = "${site.ip:0.0.0.0}",autoRefreshed = true)
    private String siteIp;

    @NacosValue(value = "${site.name:null}",autoRefreshed = true)
    private String siteName;

    @ApiOperation(value = "获取网站信息")
    @GetMapping(value = "/getSiteInfo")
    public String getSiteInfo(){
        return "MySite:"+siteName + ",ip:"+siteIp;
    }

    @ApiOperation(value = "获取实例信息")
    @ApiImplicitParam(value = "serviceName",example = "nacos-service",required = true)
    @GetMapping(value = "/getServiceInfo")
    public List<Instance> getInstanceInfo(@RequestParam(value = "serviceName") String serviceName){
        try {
            return namingService.getAllInstances(serviceName) ;
        } catch (Exception e){
            e.printStackTrace();
        }
        return null ;
    }
}
